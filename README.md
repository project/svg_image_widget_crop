# SVG Image Widget Crop

Module essentially skips or excludes SVG images when using the Image
Widget Crop module, as they can't be cropped.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/svg_image_widget_crop).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/svg_image_widget_crop).


## Requirements

- Drupal 8.8.x or 9.x or 10.x
- [Image Widget Crop](https://www.drupal.org/project/image_widget_crop)
- [SVG Image](https://www.drupal.org/project/svg_image)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module has no configuration provided.


## Maintainers

- George Anderson - [geoanders](https://www.drupal.org/u/geoanders)
- Michael O'Hara - [mikeohara](https://www.drupal.org/u/mikeohara)
