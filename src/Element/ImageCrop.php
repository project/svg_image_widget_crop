<?php

namespace Drupal\svg_image_widget_crop\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\image_widget_crop\Element\ImageCrop as IWCImageCrop;

/**
 * Provides a form element for crop.
 *
 * @FormElement("image_crop")
 */
class ImageCrop extends IWCImageCrop {

  /**
   * {@inheritdoc}
   */
  public static function processCrop(array &$element, FormStateInterface $form_state, array &$complete_form) {
    // Make sure svg_image module exists.
    if (\Drupal::moduleHandler()->moduleExists('svg_image')) {
      // See if file is svg image.
      /** @var \Drupal\file\Entity\File $file */
      $file = $element['#file'];
      if ($file !== NULL && strpos($file->getMimeType(), 'image') !== FALSE && strpos($file->getMimeType(), 'image/svg') !== FALSE) {
        // Skip svg file.
        return $element;
      }
    }

    // Process crop otherwise.
    return parent::processCrop($element, $form_state, $complete_form);
  }

}
